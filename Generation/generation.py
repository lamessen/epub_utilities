#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import shutil
import datetime
import trans_txt

def ini_struct(rep):
    ''' creation de la structure du fichier '''
    print('Génération de la stucture dans le répertoire ' + rep)

    if not os.path.exists(rep):
        os.makedirs(rep)
        
    # Génération de la structure OPS
    OPS = os.path.join(rep,'OPS')
    os.makedirs(os.path.join(OPS,'images'))
    os.makedirs(os.path.join(OPS,'textes'))
    
    source = os.path.join(os.path.dirname(os.path.abspath(__file__)),'styles')
    shutil.copytree(source,os.path.join(OPS,'styles'))
    
    # Génération du fichier META INF
    os.makedirs(os.path.join(rep,'META-INF'))
    with open(os.path.join(rep,'META-INF','container.xml'),'w') as file:
        file.write('<?xml version="1.0"?>\n')
        file.write('<container version="1.0" xmlns="urn:oasis:names:tc:opendocument:xmlns:container">\n')
        file.write('   <rootfiles>')
        file.write('     <rootfile full-path="OPS/content.opf" media-type="application/oebps-package+xml"/>\n')
        file.write('   </rootfiles>\n')
        file.write('</container>')
        
    # Génération du fichier mimetype
    with open(os.path.join(rep,'mimetype'),'w') as file:
        file.write('application/epub+zip')

def new_chap(rep,fichier,path_registre):
    '''transformation du fichier md 'fichier'  en fichier xhtml sur le bon format, <p> inclus, sur le rep epub'''

    # Récupération des données à assurer
    if not os.path.isfile(fichier):
        print("Le fichier à convertir n'existe pas")
        return
    else:
        print('Création du chapitre ' + os.path.basename(fichier).split('.')[0])

    with open(fichier,'r') as file:
        data = file.readlines()
    data = [x.rstrip('\n') for x in data]
    
    # nettoyage des données de data
    data = trans_txt.clean_txt(data,path_registre)
    data = trans_txt.conv_md(data)

    with open(os.path.join(rep,'OPS','textes',os.path.basename(fichier).split('.')[0] + '.xhtml'),'w') as file:

        # Ecriture des entête et ouverture body
        file.write('<?xml version="1.0" encoding="utf-8"?>\n')
        file.write('<!DOCTYPE html>\n')
        file.write('<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="fr" lang="fr">\n')
        file.write('  <head>\n')
        file.write('    <title>' + input('Titre du chapitre = ') + '</title>\n')
        file.write('    <link rel="stylesheet" href="../styles/framabook.css" type="text/css" />\n')
        file.write('  </head>\n')
        file.write('  <body epub:type="bodymatter chapter">\n')

        # Ecriture des lignes
        for line in data:
            file.write('  <p>' + line + '</p>\n')

        # Ecriture de la fin de fichier
        file.write('  </body>\n')
        file.write('</html>\n')
        
def clean_chap(fichier,path_registre):
    ''' correction d'un fichier xhtml en suivant les différents registres'''
    if not os.path.isfile(fichier):
        print("Le fichier à nettoyer n'existe pas")
        return
    
    # Récupération du fichier à relire
    with open(fichier,'r') as file:
        data = file.readlines()
    data = [x.rstrip('\n') for x in data]
    
    # Calcul de l'indice (on ne touche pas à l'entête)
    for ind in range(0,len(data)):
        if 'body' in data[ind]:
            break
    
    nettoye = trans_txt.clean_txt(data[ind+1:-2],path_registre)
    data = data[0:ind+1] + nettoye + data[-2:len(data)]
    
    # Ecriture du fichier
    with open(fichier,'w') as file:
        for elem in data:
            file.write(elem + '\n')
    print('Le fichier ' + fichier + 'a été corrigé')
    

def gen_struct(rep):
    '''génération de la structure d'epub dans le repertoire :
        création des fichiers ossatures de l'epub
    '''
    
    # lecture des fichiers à inclure
    liste = [os.path.join(path, x) for path, subpath, filenames in os.walk(rep) for x in filenames]
    liste = [x for x in liste if not '.git' in x]
    liste = [x.replace(rep,'') for x in liste]
    liste.sort()

    # données communes
    titre = input('Titre = ')
    identifier = input('DC:identifier = ')
    
    # Récupération de l'ordre
    path_ordre = os.path.join(os.path.dirname(os.path.abspath(__file__)),'registres','ordre_lecture.ini')
    path_ordre = [x for x in liste if not '#' in path_ordre]

    if not os.path.isfile(path_ordre):
        print('[warn] le fichier ordre_lecture.ini est nécessaire pour générer la structure epub')
        return
            
    with open(path_ordre,'r') as file:
        ordre = file.readlines()     
    try:
        len(ordre)
    except TypeError:
        print('[warn] le fichier ordre_lecture.ini n''est pas fonctionnel')
        return
        
    # Contrôle : ordre ne doit contenir que des fichiers existants dans le répertoire
    ordre_valid = [elem for elem in ordre if any([elem in x for x in liste])]
    if len(ordre_valid) != len(ordre):
        print('[warn] Fichier ordre_lecture.ini non cohérent')
        return

    # Génération du fichier content
    with open(os.path.join(rep,'OPS','content.opf'),'w') as file:
        
        # entête
        file.write('<?xml version="1.0" encoding="utf-8" standalone="yes"?>\n')
        file.write('<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="3.0">\n\n')
        file.write('  <metadata xmlns:dc="http://purl.org/dc/elements/1.1/">\n')
        file.write('    <dc:identifier id="uuid_id">' + identifier + '</dc:identifier>\n')
        file.write('    <dc:title>' + titre + '</dc:title>\n')
        file.write('    <dc:creator>' + input('Auteur = ') + '</dc:creator>\n')
        file.write('    <dc:language>fr</dc:language>\n')
        file.write('    <dc:date>' + str(datetime.date.today()) + '</dc:date>\n')
        file.write('    <meta property="dcterms:modified">' + str(datetime.datetime.today()) + '</meta>\n')
        file.write('    <dc:description>' + input('Description = ') + '</dc:description>\n')
        file.write('    <dc:publisher>Framasoft</dc:publisher>\n')
        file.Write('    <dc:rights> Licence ' + input('Licence = ') + '</dc:rights>\n')
        file.write('  </metadata>\n\n')

        # Manifest
        file.write('  <manifest>\n')
        
        for elem in liste:
            # définition du type de média
            if os.path.basename(elem).split('.')[1] == 'xml' or 'xhtml':
                media = '  media-type = "application/xhtml+xml" />'
            elif os.path.basename(elem).split('.')[1] == 'jpg' or 'jpeg':
                media = '  media-type = "image/jpeg" />'
            elif os.path.basename(elem).split('.')[1] == 'png':
                media = '  media-type = "image/png" />'
            elif os.path.basename(elem).split('.')[1] == 'css':
                media = '  media-type="text/css" />'
            elif os.path.basename(elem).split('.')[1] == 'ncx':
                media = '  media-type = "application/x-dtbncx+xml" />'
            
            # definition des autres paramètres
            href = '  href = "' + elem.strip('/') + '"'
            id = os.path.basename(elem).split('.')[0]
            
            if os.path.basename(elem) == 'toc.xhtml':
                href += '  properties = "nav"'
                id = 'nav'
                
            # écriture
            file.write('  <item id="' + id + href + media +'\n')
        file.write('  </manifest>\n')

        # spine toc
        file.write('  <spine toc="ncx">\n')
        
        for line in ordre_valid:
            file.write('    <itemref idref="' + line + '" />)\n')
        file.write('  </spine>\n\n')

        # guide
        file.write('  <guide>\n')
        file.write('    <reference href="' + [x for x in liste if ordre[0] in x][0].strip('/OPS/') +'" type="cover" title="Cover" />\n')
        file.write('  </guide>\n')
        
    # Génération de toc.ncx
    with open(os.path.join(rep,'OPS','toc.ncx'),'w') as file:

        # entête
        file.write('<?xml version="1.0" encoding="utf-8"?>\n')
        file.write('<ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1" xml:lang="fr">\n\n')
        file.write('<head>\n')
        file.write('  <meta content="' + identifier + '" name="dtb:uid"/>\n')
        file.write('  <meta content="4" name="dtb:depth"/>\n')
        file.write('  <meta content="0" name="dtb:totalPageCount"/>\n')
        file.write('  <meta content="0" name="dtb:maxPageNumber"/>\n')
        file.write('<head>\n')

        file.write('<docTitle>\n')
        file.write('<text>' + titre + '</text>\n')
        file.write('</docTitle>\n')

        file.write('<navMap>\n')
        playorder = 1
        for element in ordre :
            file.write('     <navPoint id="' + elem +'" playOrder="' +  playorder + '">\n')
            file.write('      <navLabel>\n')
            file.write('        <text>' + elem + '</text>\n')
            file.write('      </navLabel>\n')
            file.write('      <content src="' + [x for x in liste if elem in x][0].strip('/OPS/') + '"/>\n')
            file.write('    </navPoint> \n')
            playorder += 1

        file.write('</navMap>\n')
        file.write('</ncx>\n')

    # Génération du toc.xhtml
    with open(os.path.join(rep,'OPS','toc.xhtml'),'w') as file:

        # entête
        file.write('<?xml version="1.0" encoding="utf-8"?>\n')
        file.write('<!DOCTYPE html>\n')
        file.write('<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="fr" lang="fr">\n')
        file.write('  <head>\n')
        file.write('    <title>Table des matières</title>\n')
        file.write('    <link rel="stylesheet" href="./styles/framabook.css" type="text/css" />\n')
        file.write('  </head>\n')

        # corps
        file.write('  <body epub:type="backmatter">\n')
        file.write('    <nav epub:type="toc" class="epub-toc">\n')
        file.write('      <h2 id="toc" class="titre-chapitre">Table des matières</h2>\n\n')

        file.write('      <ol>\n')        
        for elem in ordre:
            file.write('        <li class="index-alpha"><a href="' + [x for x in liste if elem in x][0].strip('/OPS/') + '">' + elem +'</a></li>\n')
        file.write('      </ol>\n')

        file.write('    </nav>\n')
        file.write('  </body>\n')
        file.write('</html>\n')
        
##############################

if __name__ =='__main__':
    
    # emplacement du registre local par défaut
    path_registre = os.path.join(os.path.dirname(os.path.abspath(__file__)),'registre','registre_local.ini')
