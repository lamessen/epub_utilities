# -*- coding: utf-8 -*-
"""
Traitement d'un ficher data pour nettoyage
"""

import csv
import os
import re

def clean_txt(data,path_registre):
    '''
    nettoyage des data à l'aide du registre par défaut ou celui indiqué en argument
        data : liste des éléments à nettoyer
        path_registre (optionnel) : adresse du registre particulier
    '''
    registre = [] 

    # lecture du registre local
    if os.path.isfile(path_registre):
        with open(path_registre,'r') as file:
            reader = csv.reader(file,delimiter='\t')
            for rows in reader:
                registre.append((rows[0],rows[1]))
    else:
        print('Pas de registre dédié')

    # lecture du registre générique
    reg_gen = os.path.join(os.path.dirname(os.path.abspath(__file__)),'registre','registre_general.ini')
    if os.path.isfile(reg_gen):
        with open(reg_gen,'r') as file:
            reader = csv.reader(file,delimiter='\t')
            for rows in reader:
                print(rows)
                registre.append((rows[0],rows[1]))
    else:
        print('Pas de registre générique (registre_general.ini)')

    # sortie
    if not registre:
        print('Pas de registre disponible, aucune intervention')
    else:
        # Nettoyage des datas
        for i in range(0,len(data)):
            for reg in registre:
                data[i] = data[i].replace(reg[0],reg[1])
                
    # Lecture et traitement des mots à repérer manuellement
    reg_warn = os.path.join(os.path.dirname(os.path.abspath(__file__)),'registre','registre_warn.ini')
     # sortie
    if not os.path.isfile(reg_warn):
        print('Pas de registre éléments sensibles. Pas de mises en évidences d''élements à contrôler')
    else:
        with open(reg_warn,'r') as file:
            warn = file.readlines()
        warn = [x.rstrip('\n') for x in warn]
            
         # Mise en evidence des données
        for i in range(0,len(data)):
            for reg in warn:
                data[i] = data[i].replace(reg,'<span class="warning">' + reg + '</span>')
                
    # fin de traitement
    return data

def conv_md(data):
    ''' converti la liste data extraite du md pour mettre les balises html
        Prend en charge les titres, le gras et les italiques
    '''
    # Boucle sur data
    for i in range(0,len(data)):

        # Balisage du texte
        lvl_titre = data[i].find('# ')
        if lvl_titre != -1:
            data[i] = '<h' + str(lvl_titre+1) +'>' + data[i] + '</h' + str(lvl_titre+1) +'>'
        else:
            data[i] = '<p>' + data[i] + '</p>'
               
        # Transformation des italiques et gras
        data[i] = re.sub(r'*(.*)*', r'<em>\1</em>', data[i])
        data[i] = re.sub(r'**(.*)**', r'<strong>\1</strong>', data[i])

    # fin de traitement
    return data