#/bin/sh

# Ce scrip permet l'installation simplifiée de l'epub checker
# une fois installé, utiliser simplement la commande epub dans le dossier de l'epub pour lancer l'application

# Décompression d'epubcheck et copie du script
unzip epubcheck-4.0.1.zip -d ~/Applications/
cp epub.sh ~/bin

echo -e "\n# alias pour outil de création epub" >> ~/.bashrc
echo "alias epub='~/bin/epub.sh'" >> ~/.bashrc

echo "###### Installation terminée. #####"
echo "Redémarrer le terminal avant la première utilisation"
echo "Taper 'epub' dans le répertoire de travail pour lancer le script"
