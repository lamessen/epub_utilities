#!/bin/bash
echo " _____________________"
echo " |   Epub Utilities  |"
echo " |___________________|"
echo ""
PS3='Action à effectuer : '
options=("Générer l'epub" "Contrôle de validité" "Générer et Contrôler" "Quitter")
select opt in "${options[@]}"
do
   case $opt in
        "Générer l'epub")
            echo -n "Nom de l'epub: "
            read name
            zip -rX $name.epub mimetype META-INF OPS
	    ;;
        "Contrôle de validité")
            echo -n "Nom de l'epub: "
            read name
            java -jar ~/Applications/epubcheck-4.0.1/epubcheck.jar $name.epub -out validation.xml
            ;;
        "Générer et Contrôler")
            echo -n "Nom de l'epub: "
            read name
            zip -rX $name.epub mimetype META-INF OPS
            java -jar ~/Applications/epubcheck-4.0.1/epubcheck.jar $name.epub -out validation.xml
	    ;;
        "Quitter")
            echo "See you"
            exit
            ;;
        *) echo invalid option;;
    esac
done
