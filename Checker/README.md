# Epub utilities

Script d'installation (avec génération d'alias dans le bashrc) et de facilitation d'utilisation de du [validateur epub](https://github.com/IDPF/epubcheck). Aucune modification n'a été apportée au validateur.

# Installation

1. Télécharger le dossier complet, et lancer le script d'installation install.sh
2. Placez-vous dans le dossier de votre epub et lancez la commande "epub" pour lancer le menu


# Fonctions proposées

## Générer l'epub

Permet la génération de l'epub (le nom à donner sera demandé)

## Générer et contrôler

Permet de générer l'epub et de lancer le validateur sur le fichier généré. Le fichier de sortie du validateur sera ajouté au dossier

## Contrôle de validité

Permet de lancer le validateur sur un epub déjà généré
